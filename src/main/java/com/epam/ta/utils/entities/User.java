package com.epam.ta.utils.entities;

public class User {
    private String userEmail;
    private String userPSW;

    public User(){
        super();
    }

    public User(String userEmail, String userPSW){
        this.userEmail = userEmail;
        this.userPSW = userPSW;
    }

    public void setUserName(String userEmail) {
        this.userEmail = userEmail;
    }

    public void setUserPSW(String userPSW) {
        this.userPSW = userPSW;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserPSW() {
        return userPSW;
    }

    @Override
    public String toString() {
        return "User{" +
                "userEmail='" + userEmail + '\'' +
                ", userPSW='" + userPSW + '\'' +
                '}';
    }
}
