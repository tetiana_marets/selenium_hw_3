package com.epam.ta.utils.readers;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {
    private static final String PATH_TO_PROPERTIES_FILE = "./src/main/resources/path.properties";
    private static String userData;
    public static Properties properties;

    public PropertyReader(){
        properties = new Properties();
        readProperties();
    }

    private void readProperties(){
        try {
            properties.load(new FileInputStream(PATH_TO_PROPERTIES_FILE));
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
    }

    public String getPathToUserData (){
        return properties.getProperty("userdata");
    }

}
