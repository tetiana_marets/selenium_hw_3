package com.epam.ta.utils.readers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.epam.ta.utils.entities.User;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JsonReader {

    public static List<User> readUserFromJson(){
        ObjectMapper objMapper = new ObjectMapper();
        User[] users = new User[0];
        try{
            File dataUserFile = new File (new PropertyReader().getPathToUserData());
            users = objMapper.readValue(dataUserFile,User[].class);
            System.out.println(Arrays.asList(users));
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        return Arrays.asList(users);
    }
}
