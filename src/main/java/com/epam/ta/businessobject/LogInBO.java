package com.epam.ta.businessobject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.ta.pageobject.GmailLoginPage;
import com.epam.ta.utils.entities.User;

public class LogInBO {
    private GmailLoginPage gmailLoginPage;
    private static Logger log = LogManager.getLogger(LogInBO.class);

    public LogInBO(){
        gmailLoginPage = new GmailLoginPage();
    }
    public void logIn(User user){
        log.info("LogIn with user email " + user.getUserEmail());
        gmailLoginPage.typeUserEmail(user.getUserEmail());
        gmailLoginPage.submitEmail();
        log.info("User password is " + user.getUserPSW());
        gmailLoginPage.typeUserPassword(user.getUserPSW());
        gmailLoginPage.submitPassword();
    }
}
