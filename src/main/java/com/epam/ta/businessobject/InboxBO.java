package com.epam.ta.businessobject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.ta.pageobject.GmailInboxPage;

public class InboxBO {
    private static final Logger log = LogManager.getLogger(InboxBO.class);
    private GmailInboxPage gmailInboxPage;

    public InboxBO(){
        gmailInboxPage = new GmailInboxPage();
    }
    public void deleteEmails(int numberOfEmails){
        log.info("Select first " + numberOfEmails + " emails for deleting");
        gmailInboxPage.selectEmailCheckBox(numberOfEmails);
        log.info("Delete selected emails");
        gmailInboxPage.deleteEmails();
    }
    public void cancelDeleteSelectedEmails(){
        log.info("Cancel deleting selected emails");
        gmailInboxPage.undoDeleteEmail();
    }

    public boolean isInboxListChanged() {
        boolean isListNotChanged = gmailInboxPage.initialEmailList.equals(gmailInboxPage.getAllEmails());
        log.info("Inbox emails list is the same as before deleting " + isListNotChanged);
       return isListNotChanged;
    }
}
