package com.epam.ta.driverfactory;

import org.openqa.selenium.WebDriver;

public class WebDriverManager {
    private static ThreadLocal<WebDriver> driverPool = new ThreadLocal<>();

   private WebDriverManager(){

   }

   public static WebDriver getDriver(WebDriverType webDriverType){
       if (driverPool.get() == null) {
           driverPool.set(new WebDriverFactory().createDriver(webDriverType));
       }
       return driverPool.get();
   }

   public static void quitDriver(){
       if (driverPool != null) {
           driverPool.get().quit();
           driverPool.set(null);
       }
   }
}
