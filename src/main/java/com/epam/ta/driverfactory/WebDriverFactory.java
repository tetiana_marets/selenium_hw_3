package com.epam.ta.driverfactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.concurrent.TimeUnit;

public class WebDriverFactory {
    private static final int implicitlyWaitTime = 10;
    private static final TimeUnit timeUnit = TimeUnit.SECONDS;

    public WebDriver createDriver(WebDriverType webDriverType) {
        WebDriver webDriver;
        if (webDriverType == WebDriverType.CHROME) {
            System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver");
            webDriver = new ChromeDriver();
            webDriver.manage().timeouts().implicitlyWait(implicitlyWaitTime,timeUnit);
            webDriver.manage().window().maximize();
            webDriver.get("https://accounts.google.com/AccountChooser?service=mail&continue=https://mail.google.com/mail/");
        } else {
            throw new NotImplementedException();
        }
        return webDriver;
    }
}
