package com.epam.ta.driverfactory;

public enum WebDriverType {
    CHROME,
    FIREFOX
}
