package com.epam.ta.pageobject;

import com.epam.ta.driverfactory.WebDriverManager;
import com.epam.ta.driverfactory.WebDriverType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

public abstract class AbstractPage {
    private  final Logger log = LogManager.getLogger(AbstractPage.class);
    AbstractPage(){
        log.info("Initialize PageFactory for " + this.getClass().getSimpleName());
        PageFactory.initElements(WebDriverManager.getDriver(WebDriverType.CHROME),this);
    }
}
