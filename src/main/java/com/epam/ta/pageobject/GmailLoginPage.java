package com.epam.ta.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailLoginPage extends AbstractPage{
    @FindBy(id = "identifierId")
    private WebElement emailTextBox;

    @FindBy(xpath = "//*[@id=\"identifierNext\"]/div/button/div[2]")
    private WebElement submitEmail;

    @FindBy(name = "password")
    WebElement passwordTextBox;

    @FindBy(css = "#passwordNext > div > button > div.VfPpkd-RLmnJb")
    WebElement submitPassword;

    public void typeUserEmail(String email){
        emailTextBox.sendKeys(email);
    }

    public void submitEmail(){
        submitEmail.click();
    }

    public void typeUserPassword (String password){
        passwordTextBox.sendKeys(password);
    }

    public void submitPassword () {
        submitPassword.click();
    }

}
