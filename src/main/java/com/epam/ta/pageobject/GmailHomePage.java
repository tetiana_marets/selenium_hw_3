package com.epam.ta.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailHomePage extends AbstractPage{
    @FindBy(xpath = "/html/body/div[2]/div[1]/div[4]/ul[1]/li[2]/a")
    private WebElement signInLink;

    public void clickSignInLink(){
        signInLink.click();
    }
}
