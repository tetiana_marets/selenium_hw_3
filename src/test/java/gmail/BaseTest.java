package gmail;

import com.epam.ta.driverfactory.WebDriverManager;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import com.epam.ta.utils.entities.User;
import com.epam.ta.utils.readers.JsonReader;

import java.util.Iterator;
import java.util.List;

public class BaseTest {

    @DataProvider(name = "userDataProvider", parallel = true)
    protected Iterator<Object[]> users(){
        List<User> userList = JsonReader.readUserFromJson();
        return userList.stream().map(user -> new Object[]{user}).iterator();
    }

    @AfterMethod
    public void freeResource() {
        WebDriverManager.quitDriver();
    }
}
