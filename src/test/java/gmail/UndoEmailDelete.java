package gmail;

import com.epam.ta.businessobject.InboxBO;
import com.epam.ta.businessobject.LogInBO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.epam.ta.utils.entities.User;

public class UndoEmailDelete extends BaseTest{
    private int numberOfEmailToDelete;

    @BeforeTest
    public void setUp(){
        numberOfEmailToDelete = 3;
    }

    @Test (dataProvider = "userDataProvider")
    public void undoEmailDelete(User user){
        LogInBO logInBO = new LogInBO();
        logInBO.logIn(user);

        InboxBO inboxBO = new InboxBO();
        inboxBO.deleteEmails(numberOfEmailToDelete);
        inboxBO.cancelDeleteSelectedEmails();
        Assert.assertTrue(inboxBO.isInboxListChanged(),"Inbox list of emails has been changed");
        }
    }
